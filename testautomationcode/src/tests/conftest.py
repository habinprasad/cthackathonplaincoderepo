import pytest

from testautomationcode.src.main.apis import APIs

@pytest.fixture(scope='class')
def api_setup(requests):
    requests.cls.apis = APIs()