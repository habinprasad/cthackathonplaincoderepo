"""
This consists of all the APIs
"""
import os

import requests

from testautomationcode.src.utilities import constants


class APIs:

    def get_all_users(self):
        url = os.path.join(constants.API_URL, 'api/v2/users')
        response = requests.get(url=url)
        return response.json()

    def post_register_user(self, usr_meta):
        url = os.path.join(constants.API_URL, 'api/v2/addusers')
        response = requests.post(url=url, data=usr_meta)
        return response.json()

    def update_existing_user(self, usr_meta):
        url = os.path.join(constants.API_URL, 'api/v2/updateuser')
        response = requests.put(url=url, data=usr_meta)
        return response.json()

    def delete_user(self, usr_id):
        url = os.path.join(constants.API_URL, f'/api/v2/{usr_id}/deleteuser')
        response = requests.delete(url=url)
        return response.json()